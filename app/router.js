'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.get('/', controller.home.index);
  router.post('/api/add_user', controller.lottery.addUser);// 新增用户
  router.get('/api/user', controller.lottery.user);// 获取用户信息
  router.get('/api/pool_awards', controller.lottery.poolAwards);// 获取奖池奖品信息
  router.get('/api/draw', controller.lottery.draw);// 抽奖
  router.post('/api/add_award', controller.lottery.addAward);// 管理页面，添加一个奖品
  router.post('/api/edit_pool', controller.lottery.editPool);// 管理页面，配置奖池项目
  router.post('/api/upload', controller.lottery.upload);// 上传文件
  router.post('/api/edit_cost', controller.lottery.editCost);// 上传文件
};
