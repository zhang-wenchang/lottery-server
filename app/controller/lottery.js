'use strict';

const Controller = require('egg').Controller;
const fs = require('mz/fs');
const path = require('path');
const os = require('os');

const successMsg = '请求成功';
const failMsg = '请求失败';
const successCode = 200;
const failCode = 500;


let uploadDir;
if (os.type() !== 'Linux') {
  uploadDir = 'D:\\上传文件';
} else {
  uploadDir = '/root/data/upload';
}
class LotteryController extends Controller {
  async addUser() {
    const { ctx } = this;
    const { user_name, ore_num } = ctx.request.body;
    try {
      await ctx.service.lottery.addUser(user_name, parseInt(ore_num));
      ctx.body = {
        code: successCode,
        msg: successMsg,
      };
    } catch (error) {
      ctx.body = {
        code: failCode,
        msg: failMsg,
      };
    }
  }
  async user() {
    const { ctx } = this;
    const { id } = ctx.query;
    const user = await ctx.service.lottery.user(id);
    const history = await ctx.service.lottery.userHistory(id);
    ctx.body = {
      code: successCode,
      msg: successMsg,
      data: {
        user,
        history,
      },
    };
  }
  async poolAwards() {
    const { ctx } = this;
    const { mode } = ctx.query;
    const result = await ctx.service.lottery.poolAwards(mode ? parseInt(mode) : 1);
    const config = await ctx.service.lottery.getConfig();
    ctx.body = {
      code: successCode,
      msg: successMsg,
      data: {
        awards: result,
        cost: config.cost,
      },
    };
  }
  async draw() {
    const { ctx } = this;
    const { userid } = ctx.query;
    const result = await ctx.service.lottery.draw(userid);
    ctx.body = {
      code: successCode,
      msg: successMsg,
      data: result,
    };
  }
  async addAward() {
    const { ctx } = this;
    const { award_name, image_type } = ctx.request.body;
    try {
      await ctx.service.lottery.addAward(award_name, image_type);
      ctx.body = {
        code: successCode,
        msg: successMsg,
      };
    } catch (error) {
      ctx.body = {
        code: failCode,
        msg: failMsg,
      };
    }
  }
  async editPool() {
    const { ctx } = this;
    const { number, award_id, award_name, award_image, probability, type } = ctx.request.body;
    try {
      await ctx.service.lottery.editPool(parseInt(number), award_id, award_name, award_image, parseInt(probability), type);
      ctx.body = {
        code: successCode,
        msg: successMsg,
      };
    } catch (error) {
      ctx.body = {
        code: failCode,
        msg: failMsg,
      };
    }
  }

  async upload() {
    const { ctx } = this;
    const file = ctx.request.files[0];
    const { award_id } = ctx.request.body;
    try {
      fs.accessSync(uploadDir);
    } catch (err) {
      fs.mkdirSync(uploadDir, { recursive: true });
    }

    const fileType = path.extname(file.filename);
    const image_type = fileType.substring(1);
    const destination = path.format({
      dir: uploadDir,
      base: award_id + fileType,
    });
    try {
      fs.copyFileSync(file.filepath, destination);
      await ctx.service.lottery.addAward(award_id, null, image_type);
    } catch (err) {
      ctx.body = {
        code: failCode,
        msg: err,
      };
      return;
    } finally {
      // 需要删除临时文件
      await fs.unlink(file.filepath);
    }

    ctx.body = {
      code: successCode,
      msg: successMsg,
      data: {
        url: global.IMG_PATH_PREFIX + award_id + '.' + image_type,
      },
    };
  }

  async editCost() {
    const { ctx } = this;
    const { cost } = ctx.request.body;
    try {
      await ctx.service.lottery.editCost(parseInt(cost));
      ctx.body = {
        code: successCode,
        msg: successMsg,
        data: {
          cost,
        },
      };
    } catch (error) {
      ctx.body = {
        code: failCode,
        msg: failMsg,
      };
    }
  }

}

module.exports = LotteryController;
