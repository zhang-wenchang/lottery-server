'use strict';

const Controller = require('egg').Controller;

class HomeController extends Controller {
  async index() {
    const { ctx } = this;
    ctx.body = '抽奖小程序首页，请按照接口文档访问对应的接口地址';
  }
}

module.exports = HomeController;
